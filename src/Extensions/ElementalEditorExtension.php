<?php

namespace HotHouse\ElementalExtendedGrid\Extensions;

use SilverStripe\Core\Config\Config;
use SilverStripe\Forms\HiddenField;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Security\Permission;
use SilverStripe\View\Requirements;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\DropdownField;


class ElementalEditorExtension extends DataExtension {

    /**
     * @param \SilverStripe\Forms\GridField\GridField $gridField
     */
    public function updateField($gridField){
        //Require extra resources
        Requirements::css('hothouse/silverstripe-elemental-extended-grid:resources/css/elementalgrid.css');
        Requirements::javascript('hothouse/silverstripe-elemental-extended-grid:resources/js/elementalgrid.js');

        //Change the config
        $config = $gridField->getConfig();
        $config->getComponentByType(GridFieldOrderableRows::class)->setImmediateUpdate(false);

        //Add editable columns
        $defaultSizeField = 'Size' . Config::forClass('TheWebmen\ElementalGrid')->get('defaultSizeField');
        $defaultSizeFieldTitle = str_replace('Size', 'Size ', $defaultSizeField);
        $defaultSizeFieldTranslateKey = strtoupper(str_replace(' ', '_', $defaultSizeFieldTitle));

        $defaultOffsetField = 'Offset' . Config::forClass('TheWebmen\ElementalGrid')->get('defaultOffsetField');
        $defaultOffsetFieldTitle = str_replace('Size', 'Size ', $defaultOffsetField);
        $defaultOffsetFieldTranslateKey = strtoupper(str_replace(' ', '_', $defaultOffsetFieldTitle));

        $editableColumns = new \Symbiote\GridFieldExtensions\GridFieldEditableColumns();
        $editableColumns->setDisplayFields(array(
            'SizeMD' => array(
                'title' => _t('HotHouse\ElementalExtendedGrid\Extensions\BaseElementExtension.SIZE_MD', 'Size MD'),
                'callback' => function($record, $column, $grid) {
                    return DropdownField::create('SizeMD', _t('HotHouse\ElementalExtendedGrid\Extensions\BaseElementExtension.SIZE_MD', 'Size MD'), BaseElementExtension::getColSizeOptions())->addExtraClass('grideditor-sizefield')->setAttribute('data-title', _t('HotHouse\ElementalExtendedGrid\Extensions\BaseElementExtension.SIZE_MD', 'Size MD'));
                }
            ),
            'OffsetMD' => array(
                'title' => _t('HotHouse\ElementalExtendedGrid\Extensions\BaseElementExtension.OFFSET_MD', 'Offset MD'),
                'callback' => function($record, $column, $grid) {
                    return DropdownField::create('OffsetMD', _t('HotHouse\ElementalExtendedGrid\Extensions\BaseElementExtension.OFFSET_MD', 'Offset MD'), BaseElementExtension::getColSizeOptions(false, true))->addExtraClass('grideditor-offsetfield')->setAttribute('data-title', _t('HotHouse\ElementalExtendedGrid\Extensions\BaseElementExtension.OFFSET_MD', 'Offset MD'));
                }
            ),
            'BlockType' => array(
                'callback' => function($record, $column, $grid) {
                    return HiddenField::create('BlockType', 'BlockType');
                }
            )
        ));
        $config->addComponent($editableColumns);

        //Add extra class
        $gridField->addExtraClass('grideditor');

        if (!Permission::check('EDIT_ELEMENTALS')) {
            $gridField->addExtraClass('hidden');
        }
    }

}
